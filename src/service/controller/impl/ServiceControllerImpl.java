package service.controller.impl;


import message.HistoryMessage;
import message.OfflineMessage;
import message.OnlineMessage;
import service.controller.dao.ServiceControllerDAO;
import user.User;
import user.UserDocument;
import user.UserInformation;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;


/**
 * @author 黄泓彬
 */
@SuppressWarnings("ALL")
public class ServiceControllerImpl implements ServiceControllerDAO {

    private static Map<String, ThreadServer> threadMap = new HashMap<>();
    static InputStream inputStream;
    static OutputStream outputStream;
    static ObjectInputStream objectInputStream;
    static ObjectOutputStream objectOutputStream;
    @Override
    public Connection connectToMysql() throws SQLException {
        String driverName="com.mysql.cj.jdbc.Driver";
        String dataBaseUrl="jdbc:mysql://139.9.128.192:3306/java?useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true";
        String userName="huanghongbe";
        String userPassWord="123456";
        Connection connection;
        try{
            Class.forName(driverName);
            System.out.println("加载驱动成功！");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("加载驱动失败！");
        }
        connection=DriverManager.getConnection(dataBaseUrl,userName,userPassWord);
        return connection;
    }

    public static void main(String[] args) throws Exception {
        ServiceControllerImpl serviceController = new ServiceControllerImpl();
        ServerSocket serverSocket = null;
        Socket socket;
        try {
            serverSocket = new ServerSocket(20);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Object o = null;
        while (true) {
            try {
                //assert关键字,返回一个boolean，如果为true，则程序继续进行，否则，抛出异常阻塞程序
                assert serverSocket != null;
                socket = serverSocket.accept();
                if (socket != null) {
                    inputStream = socket.getInputStream();
                    objectInputStream = new ObjectInputStream(inputStream);
                    o = objectInputStream.readObject();
                    //获取新连接的socket所需的输出流
                    outputStream = socket.getOutputStream();
                    objectOutputStream = new ObjectOutputStream(outputStream);

                    if("class user.User".equals(o.getClass().toString())){
                        User user = (User) o;
                        if (serviceController.checkIfTheAccountMatches(user)) {
                            objectOutputStream.writeObject("Login successful！");
                            objectOutputStream.flush();
                            serviceController.sendPersonName();
                            serviceController.sendOfflineMessage( user);
                            serviceController.creatThread(user,objectInputStream,objectOutputStream);
                        } else {
                            objectOutputStream.writeObject("Login error！");
                            objectOutputStream.flush();
                        }
                    }else{
                        UserInformation userInformation=(UserInformation)o;
                        try {
                            serviceController.userRegister(userInformation);
                            objectOutputStream.writeObject("user created successfully!");
                            objectOutputStream.flush();
                        }catch (SQLException e){
                            objectOutputStream.writeObject("Find error in user regist!");
                            objectOutputStream.flush();
                        }

                    }
                }

            }catch (SQLException e){
                System.out.println("数据库异常，不能进行对"+((User) o).getUserName()+"查找匹配");
            } catch (IOException | ClassNotFoundException  e) {
                System.out.println("线程意外退出！");
            }
        }
    }
    @Override
    public  void userRegister( UserInformation userInformation) throws SQLException {
        Connection connection;
        connection=connectToMysql();
        PreparedStatement preparedStatement;
        String SQL="select* from user where userName=?";
        preparedStatement=connection.prepareStatement(SQL);
        preparedStatement.setString(1,userInformation.getUser().getUserName());

        ResultSet resultSet=preparedStatement.executeQuery();
        if(!resultSet.next()){
            SQL="CREATE TABLE "+userInformation.getUser().getUserName()+"("+"hisName varchar(20),"+"Flag Integer(2),"+"Message text(255),"+"Type Integer(2),"+"Time varchar(20)"+")charset=utf8;";
            preparedStatement.execute(SQL);

            StringBuffer stringBuffer = new StringBuffer("Insert into ");
            stringBuffer.append("user");
            stringBuffer.append(" values(?,?)");
            SQL = new String(stringBuffer);
            preparedStatement=connection.prepareStatement(SQL);
            preparedStatement.setString(1,userInformation.getUser().getUserName());
            preparedStatement.setString(2,userInformation.getUser().getPassWord());
            preparedStatement.execute();

        }
        connection.close();
        preparedStatement.close();
    }//√√√√√√
    @Override
    public  void sendPersonName() throws SQLException, IOException {
        Connection connection;
        connection=connectToMysql();
        PreparedStatement preparedStatement;
        String SQL="select* from user";
        preparedStatement=connection.prepareStatement(SQL);
        ResultSet resultSet=preparedStatement.executeQuery();
        User user;
        objectOutputStream.writeObject(new User(null,null));
        objectOutputStream.flush();
        while (resultSet.next()) {
            user = new User(resultSet.getString(1),resultSet.getString(2));
            objectOutputStream.writeObject(user);
            objectOutputStream.flush();
        }
        objectOutputStream.writeObject(new User(null,null));
        objectOutputStream.flush();
        connection.close();
        preparedStatement.close();

    }//√√√√√√

    @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
    @Override
    public void sendOfflineMessage( User user) throws IOException, SQLException {
        String userName = user.getUserName();
        Connection connection;
        connection =  connectToMysql();
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        //Flag=1是别人给我的消息  Type=1是离线消息
        StringBuffer stringBuffer = new StringBuffer("select * from ");
        stringBuffer.append(userName);
        stringBuffer.append(" where `Flag`=1 AND `Type`=1 ");
        String SQL = new String(stringBuffer);
        preparedStatement = connection.prepareStatement(SQL);

        resultSet = preparedStatement.executeQuery();
        objectOutputStream.writeObject(new OfflineMessage(null,null,null,null));
        objectOutputStream.flush();
        while (resultSet.next()) {
            String time = resultSet.getString(5);
            String hisName = resultSet.getString(1);

            String message = resultSet.getString(3);
            OfflineMessage offlineMessage = new OfflineMessage(time, hisName, userName, message);
            objectOutputStream.writeObject(offlineMessage);
            objectOutputStream.flush();
        }
        objectOutputStream.writeObject(new OfflineMessage(null,null,null,null));
        objectOutputStream.flush();
        stringBuffer = new StringBuffer("update ");
        stringBuffer.append(userName);
        stringBuffer.append(" set `Type`=2 where `Flag`=1 AND `Type`=1 ");
        SQL = new String(stringBuffer);
        preparedStatement = connection.prepareStatement(SQL);
        preparedStatement.executeUpdate();
        connection.close();
        preparedStatement.close();
    }//√√√√√
    @Override
    public boolean checkIfTheAccountMatches(User user) throws SQLException {
        String userName=user.getUserName();
        String passWord=user.getPassWord();
        PreparedStatement preparedStatement = null;
        Connection connection;
        if((connection= connectToMysql())!=null)
        //noinspection AlibabaRemoveCommentedCode
        {
            String SQL="select * from user where userName=? AND passWord=?";
            preparedStatement=connection.prepareStatement(SQL);
            preparedStatement.setString(1,userName);
            preparedStatement.setString(2,passWord);

            ResultSet resultSet;
            resultSet=preparedStatement.executeQuery();

            while(resultSet.next()){
                if(resultSet.getString(1)!=null){

                    connection.close();
                    preparedStatement.close();
                    return true;
                }
            }

            connection.close();
            preparedStatement.close();
            return false;

        }
        assert false;
        connection.close();
        assert preparedStatement != null;
        preparedStatement.close();
        return false;
    }//√√√√√
    @Override
    public void creatThread(User user,ObjectInputStream objectInputStream,ObjectOutputStream objectOutputStream) throws Exception{
        ThreadServer t = new ThreadServer(user,objectInputStream,objectOutputStream);
        Thread.sleep(10);
        t.start();
        threadMap.put(user.getUserName(), t);
    }//√√√√√√

    class ThreadServer extends Thread {
        User user;
        ObjectInputStream ois;
        ObjectOutputStream oos;

        public ThreadServer(User user, ObjectInputStream ois, ObjectOutputStream oos) {
            this.user = user;
            this.ois = ois;
            this.oos = oos;
        }

        public void getHistoryMessage() throws SQLException, IOException {
            Connection connection=connectToMysql();
            PreparedStatement preparedStatement;
            ResultSet resultSet;

            StringBuffer stringBuffer = new StringBuffer("select * from ");
            stringBuffer.append(user.getUserName());
            String SQL = new String(stringBuffer);
            preparedStatement = connection.prepareStatement(SQL);
            resultSet = preparedStatement.executeQuery();
            oos.writeObject(new HistoryMessage(null,null,null,null));
            oos.flush();
            while(resultSet.next()) {
                String hisName=resultSet.getString(1);
                int flag=resultSet.getInt(2);
                String message=resultSet.getString(3);
                String time=resultSet.getString(5);
                HistoryMessage historyMessage;
                if(flag==1){//别人给我的消息
                    historyMessage=new HistoryMessage(time,hisName,user.getUserName(),message);
                }
                else {
                    historyMessage=new HistoryMessage(time,user.getUserName(),hisName,message);
                }
                oos.writeObject(historyMessage);
                oos.flush();
            }
            oos.writeObject(new HistoryMessage(null,null,null,null));
            oos.flush();

        }

        @Override
        public void run() {
            while (true) {
                try {
                    Object o = ois.readObject();
                    if ("class message.OnlineMessage".equals(o.getClass().toString())) {
                        OnlineMessage onlineMessage = (OnlineMessage) o;
                        if (sendMessage(onlineMessage)) {
                            //正确发送，Type为2，数据库我自己的表Flag为2，别人的表Flag为1
                            insertIntoMysql(onlineMessage, 1);
                        } else {
                            //存入数据库，自己的表Type为2，对方的表Type为1，数据库我自己的表Flag为2，别人的表Flag为1
                            insertIntoMysql(onlineMessage, 2);
                        }
                    } else if ("class message.HistoryMessage".equals(o.getClass().toString())) {
                        getHistoryMessage();//发送历史消息
                    }
                    else if("class user.UserDocument".equals(o.getClass().toString()))
                    {
                        UserDocument userDocument=(UserDocument) ois.readObject();
                        sendDocument(userDocument);
                    }
                }  catch (IOException |ClassNotFoundException e) {
                    System.out.println(user.getUserName()+"线程意外退出！");
                    threadMap.remove(user.getUserName());
                    return;
                } catch (SQLException e){
                    System.out.println(user.getUserName()+"此线程数据库异常！");
                    threadMap.remove(user.getUserName());
                    return;
                }
            }


        }



        public void insertIntoMysql(OnlineMessage onlineMessage,int flag) throws SQLException {
            Connection connection;
            String SQL;
            StringBuffer stringBuffer = new StringBuffer("Insert into ");
            stringBuffer.append(onlineMessage.getSender());
            stringBuffer.append(" values(?,?,?,?,?)");
            SQL = new String(stringBuffer);

            connection=connectToMysql();
            PreparedStatement preparedStatement = null;
            /*flag为1是正确发送*/
            if(flag==1) {
                //自己的表
                //Flag=1是别人给我的消息  Type=1是离线消息
                preparedStatement=connection.prepareStatement(SQL);
                preparedStatement.setString(1,onlineMessage.getReceivingEnd());
                System.out.println(onlineMessage.getReceivingEnd());
                System.out.println(onlineMessage.getSender());
                preparedStatement.setString(2,"2");
                preparedStatement.setString(3,onlineMessage.getMessage());
                preparedStatement.setString(4,"2");
                preparedStatement.setString(5,onlineMessage.getTime());
                preparedStatement.executeUpdate();
                //对方的表
                stringBuffer = new StringBuffer("Insert into ");
                stringBuffer.append(onlineMessage.getReceivingEnd());
                stringBuffer.append(" values(?,?,?,?,?)");
                SQL = new String(stringBuffer);
                preparedStatement=connection.prepareStatement(SQL);
                preparedStatement.setString(1,onlineMessage.getSender());
                preparedStatement.setString(2,"1");
                preparedStatement.setString(3,onlineMessage.getMessage());
                preparedStatement.setString(4,"2");
                preparedStatement.setString(5,onlineMessage.getTime());
                preparedStatement.executeUpdate();

            }
            else {
                //自己的表
                //Flag=1是别人给我的消息  Type=1是离线消息
                preparedStatement=connection.prepareStatement(SQL);
                preparedStatement.setString(1,onlineMessage.getReceivingEnd());
                preparedStatement.setString(2,"2");
                preparedStatement.setString(3,onlineMessage.getMessage());
                preparedStatement.setString(4,"2");
                preparedStatement.setString(5,onlineMessage.getTime());
                preparedStatement.executeUpdate();
                //对方的表
                stringBuffer = new StringBuffer("Insert into ");
                stringBuffer.append(onlineMessage.getReceivingEnd());
                stringBuffer.append(" values(?,?,?,?,?)");
                SQL = new String(stringBuffer);
                preparedStatement=connection.prepareStatement(SQL);
                preparedStatement.setString(1,onlineMessage.getSender());
                preparedStatement.setString(2,"1");
                preparedStatement.setString(3,onlineMessage.getMessage());
                preparedStatement.setString(4,"1");
                preparedStatement.setString(5,onlineMessage.getTime());
                preparedStatement.executeUpdate();
            }
            connection.close();
            assert preparedStatement != null;
            preparedStatement.close();


        }
        public boolean sendMessage(OnlineMessage onlineMessage) throws IOException {
            if(threadMap.containsKey(onlineMessage.getReceivingEnd())){
                System.out.println(onlineMessage.getReceivingEnd());
                threadMap.get(onlineMessage.getReceivingEnd()).forwardMessage(onlineMessage);//发送给对方信息
                return true;
            }else{
                return false;
            }
        }
        public void forwardMessage(OnlineMessage onlineMessage) throws IOException {//转发功能
            oos.writeObject(new OnlineMessage(null,null,null,null));
            oos.flush();
            oos.writeObject(onlineMessage);
            oos.flush();
        }

        public boolean sendDocument(UserDocument userDocument) throws IOException {
            if(threadMap.containsKey(userDocument.getReceiver()))
            {
                threadMap.get(userDocument.getReceiver()).forwardDocument(userDocument);
                return true;
            }
            return false;
        }

        private void forwardDocument(UserDocument userDocument) throws IOException {
            oos.writeObject(new UserDocument(null,null,null));
            oos.flush();
            oos.writeObject(userDocument);
            oos.flush();
        }

    }



}

