package service.controller.dao;

import user.User;
import user.UserInformation;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author 黄泓彬
 */
public interface ServiceControllerDAO {
    /**
     * 建立数据库连接
     * @return
     * @throws SQLException
     */
    Connection connectToMysql() throws SQLException;

    /**
     *账户验证
     * @param userInformation
     * @throws SQLException
     */
    void userRegister(UserInformation userInformation) throws SQLException;

    /**
     * 返回用户列表
     * @throws SQLException
     * @throws IOException
     */
    void sendPersonName() throws SQLException, IOException;

    /**
     * 返回离线消息
     * @param user
     * @throws IOException
     * @throws SQLException
     */
    void sendOfflineMessage(User user) throws IOException, SQLException;

    /**
     *
     * @param user
     * @return
     * @throws SQLException
     */
    boolean checkIfTheAccountMatches(User user) throws SQLException;
    void creatThread(User user, ObjectInputStream objectInputStream, ObjectOutputStream objectOutputStream) throws Exception;
}
