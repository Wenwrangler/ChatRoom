package user;

import java.io.Serializable;

/**
 * @author ������
 */
public class UserInformation implements Serializable {
    User user;

    public UserInformation(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
