package user.gui;

import message.HistoryMessage;
import message.Message;
import message.OfflineMessage;
import message.OnlineMessage;
import user.controller.impl.UserController;
import user.User;
import user.UserDocument;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

/**
 * @author 陈星星
 */
public class ContactJFrame {
    private static JButton Record;
    private static String receivingEnd;
    private static String time;
    private static String sender = Login.getUserName();
    private static String message;
    private static JTextArea jEditorPane = new JTextArea();
    private static JPanel userListjPanel = new JPanel();
    private static JFrame frame = new JFrame("Contact");
    //存放用户列表的button框架
    private static JPanel TakingjPanel=new JPanel();
    private static JPanel EditorjPanel=new JPanel();
    private static ButtonGroup group = new ButtonGroup();
    private static JButton  sendButton=new JButton("发送");
    private static JButton sendFileButton =new JButton();
    public static List<JToggleButton> jToggleButtons = new ArrayList<>();
    static HashMap<String, ArrayList<Message>> hashMap = new HashMap<>();
    static List<User> userItem = new ArrayList<>();

    public static void changeButton(Message message) {
        String username = message.getSender();
        for (int i = 0; i < jToggleButtons.size(); i++) {
            if (username.equals(jToggleButtons.get(i).getText())) {
                jToggleButtons.get(i).setBackground(Color.RED);
            }
        }
    }

    public static void returnOfflineMessage(ArrayList<OfflineMessage> offlineMessages) {
        for (OfflineMessage o : offlineMessages) {
            if (hashMap.containsKey(o.getSender())) {
                hashMap.get(o.getSender()).add(o);
            } else {
                ArrayList<Message> messageArrayList = new ArrayList<>();
                messageArrayList.add(o);
                hashMap.put(o.getSender(), messageArrayList);
            }
        }
    }

    public static void returnOnlineMessage(OnlineMessage onlineMessage) {
        if(onlineMessage.getSender().equals(receivingEnd)){
            //打印到屏幕
            jEditorPane.append(onlineMessage.getSender() + " ：" + onlineMessage.getMessage() + "(" + onlineMessage.getTime() + ")" + "\n");
        }else{
            changeButton(onlineMessage);
        }
        if(hashMap.containsKey(onlineMessage.getSender())){
            hashMap.get(onlineMessage.getSender()).add(onlineMessage);
        }else{
            ArrayList<Message> tempList = new ArrayList<>();
            tempList.add(onlineMessage);
            hashMap.put(onlineMessage.getSender(),tempList);
        }
    }

    public static void setButton()

    {
        //将用户创建button
        for (int i = 0; i < userItem.size(); i++) {
            if (!userItem.get(i).getUserName().equals(sender)) {
                JToggleButton jToggleButton = new JToggleButton(userItem.get(i).getUserName());
                jToggleButtons.add(jToggleButton);
                int finalI1 = i;
                jToggleButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        jEditorPane.setText("");
                        receivingEnd = userItem.get(finalI1).getUserName();
                        for (int i = 0; i < jToggleButtons.size(); i++) {
                            if (userItem.get(finalI1).getUserName().equals(jToggleButtons.get(i).getText())) {
                                jToggleButtons.get(i).setBackground(null);
                            }
                        }
                        if (hashMap.containsKey(receivingEnd)) {
                            for (Message message : hashMap.get(receivingEnd)) {
                                if(message.getSender().equals(sender)){
                                    jEditorPane.append("我" + " ：" + message.getMessage() + "(" + message.getTime() + ")" + "\n");
                                }else {
                                    jEditorPane.append(message.getSender() + " ：" + message.getMessage() + "(" + message.getTime() + ")" + "\n");
                                }
                            }
                        }
                    }
                });
                userListjPanel.add(jToggleButton);
                group.add(jToggleButton);
                jToggleButton.setPreferredSize(new Dimension(10, 10));
            }
        }
    }
    public static void returnListInformation(ArrayList<User> users){
        userItem = users;
    }
    public static void contactJFrame() throws IOException, ClassNotFoundException {
        //System.out.println(sender);

        EditorjPanel.setPreferredSize(new Dimension(150,90));
        userListjPanel.setLayout(new GridLayout(0,1,0,3));

        Record=new JButton("聊天记录");
        //button1.setPreferredSize(new Dimension(40,40));
        JButton button=new JButton("退出登录");
        button.setContentAreaFilled(false);
        //button.setIcon(image);
        //button.setPreferredSize(new Dimension(40,40));

        userListjPanel.add(button);
        //创建滚轮条目
        JScrollPane scrollPane = new JScrollPane(
                userListjPanel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
        );
        //设置窗口大小
        scrollPane.setPreferredSize(new Dimension(150,500));
        // 创建文本区域组件

        // 设置字体
        jEditorPane.setFont(new Font(null, Font.PLAIN, 15));
        jEditorPane.setEditable(false);
        // 创建滚动面板, 指定滚动显示的视图组件(textArea), 垂直滚动条一直显示, 水平滚动条从不显示
        //显示聊天的框
        JScrollPane takingsPane1 = new JScrollPane(
                jEditorPane,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
        );
        //创建码字文本区域组件
        JTextArea textArea1 = new JTextArea();
        // 自动换行
        textArea1.setLineWrap(true);
        textArea1.setFont(new Font(null, Font.PLAIN, 15));
        //码字的框
        JScrollPane scrollPane2 = new JScrollPane(
                textArea1,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER
        );
        //scrollPane2.setBounds(10,50,400,80);
        sendFileButton.setBounds(155,345,20,20);
        sendButton.setBounds(593,425,85,30);
        Record.setBounds(593,385,85,30);
        scrollPane2.setBounds(10,380,570,80);
        frame.add(scrollPane2);
        frame.add(sendButton);
        frame.add(Record);
        frame.add(sendFileButton);
        //scrollPane2.setPreferredSize(new Dimension(400,80));
        TakingjPanel.add(takingsPane1);
        //EditorjPanel.add(BorderLayout.EAST,button1);
        //EditorjPanel.add(BorderLayout.EAST,Record);
        //EditorjPanel.add(BorderLayout.WEST,button);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(BorderLayout.WEST,scrollPane);
        frame.add(BorderLayout.CENTER,takingsPane1);
        frame.add(BorderLayout.SOUTH,EditorjPanel);
        //frame.add(BorderLayout.EAST,button);
        frame.setSize(700, 500);
        frame.setVisible(true);
        frame.setLocation(450,200);
        frame.setResizable(false);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
                frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                new user.gui.Login().setVisible(true);
            }
        });
        Record.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    UserController.requestHistoryMessage();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                /**********展示聊天记录
                 *
                 */
            }
        });

        sendFileButton.addActionListener(new ActionListener() {
            @Override
            /**发送文件
             *
             */
            public void actionPerformed(ActionEvent e) {
                //设置选择器
                JFileChooser chooser = new JFileChooser();
                //设为多选
                chooser.setMultiSelectionEnabled(true);
                //是否打开文件选择框
                int returnVal = chooser.showOpenDialog(sendFileButton);
                System.out.println("returnVal="+returnVal);
                //如果符合文件类型
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    //获取绝对路径
                    String filepath = chooser.getSelectedFile().getAbsolutePath();
                    System.out.println(filepath);
                    //输出相对路径
                    System.out.println("You chose to open this file: "+ chooser.getSelectedFile().getName());

                    String fileName=chooser.getSelectedFile().getName();
                    UserDocument userDocument= null;

                    try {
                        userDocument = UserController.getUserDocumentByPath(receivingEnd,filepath,fileName);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    try {
                        UserController.sendDocument(userDocument);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }

                }
            }
        });
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date date = new Date();
                DateFormat format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
                //2013-01-14
                time = format.format(date);
                message=textArea1.getText();
                textArea1.setText("");
                OnlineMessage onlineMessage =new OnlineMessage(time,sender,receivingEnd,message);
                if(hashMap.containsKey(onlineMessage.getReceivingEnd())){
                    hashMap.get(onlineMessage.getReceivingEnd()).add(onlineMessage);
                }else{
                    ArrayList<Message> arrayList = new ArrayList<>();
                    arrayList.add(onlineMessage);
                    hashMap.put(onlineMessage.getReceivingEnd(),arrayList);
                }

                jEditorPane.append("我："+message+"("+time+")"+"\n");
                try {
                    UserController.sendMessage(onlineMessage);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

    }
    public static void returnHistoryList(ArrayList<HistoryMessage> list){
        hashMap.clear();
        for (Message m:list) {
            if(m.getSender().equals(sender)){
                if(hashMap.containsKey(m.getReceivingEnd())){
                    hashMap.get(m.getReceivingEnd()).add(m);
                }else{
                    ArrayList<Message> tempList = new ArrayList<>();
                    tempList.add(m);
                    hashMap.put(m.getReceivingEnd(),tempList);
                }
            }else{
                if(hashMap.containsKey(m.getSender())){
                    hashMap.get(m.getSender()).add(m);
                }else{
                    ArrayList<Message> tempList = new ArrayList<>();
                    tempList.add(m);
                    hashMap.put(m.getSender(),tempList);
                }
            }
        }
        jEditorPane.setText("");
        if(receivingEnd!=null){
            for (Message m : hashMap.get(receivingEnd)) {
                if(m.getSender().equals(sender)){
                    jEditorPane.append("我" + " ：" + m.getMessage() + "(" + m.getTime() + ")" + "\n");
                }else {
                    jEditorPane.append(m.getSender() + " ：" + m.getMessage() + "(" + m.getTime() + ")" + "\n");
                }
            }
        }


    }
    /*public void actionPerformed(ActiveEvent e){
        if(e.getActionConmmand().)
    }*/


    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        while(userItem.size()==0){
            Thread.sleep(10);
        }
        contactJFrame();
    }

    public static void receiveUserDocument(UserDocument userDocument) throws IOException {
        int value= JOptionPane.showConfirmDialog(null,"是否接收文件");
        /*当选择确认再接收文件

         */
        if(value==JOptionPane.YES_OPTION) {
            StringBuffer stringBuffer=new StringBuffer("D:\\");
            stringBuffer.append(userDocument.getFileName());
            System.out.println(userDocument.getFileName());
            String filePath=new String(stringBuffer);
            System.out.println(filePath);
            File file=new File(filePath);
            FileOutputStream fileOutputStream=new FileOutputStream(file);
            DataOutputStream dataOutputStream=new DataOutputStream(fileOutputStream);
            dataOutputStream.write(userDocument.getSendbutes());
            dataOutputStream.flush();
//            BufferedOutputStream bufferedOutputStream=new BufferedOutputStream(fileOutputStream);
//            bufferedOutputStream.write(userDocument.getSendbutes());
//            bufferedOutputStream.flush();

        }
    }
}