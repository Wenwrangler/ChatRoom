package user.controller.impl;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import message.HistoryMessage;
import message.Message;
import message.OfflineMessage;
import message.OnlineMessage;
import user.gui.ContactJFrame;
import user.gui.Login;
import user.gui.RegisteJFrame;
import user.User;
import user.UserDocument;
import user.UserInformation;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author 温海杰
 */
public class UserController {
    public static final Integer PORT = 20;
    public static final String HOST = "139.9.128.192";
    static ObjectOutputStream objectOutputStream;
    static ObjectInputStream objectInputStream;
    static Socket socket;
    public static void main(String[] args)  {
        try
        {
            socket=new Socket(HOST,PORT);
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"无法连接！请检查网络！");
            return;
        }
        String[] strings = new String[10];
        Login.main(strings);
        try
        {
            objectInputStream = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"服务器异常！请稍后重试，或者联系我们Tel：17685392456");
        }
        Thread thread = new Thread(new ReceiveThread(objectInputStream));
        thread.start();
    }

    /**
     * 请求登录--发送User对象 requestLogin
     * 请求注册--发送UserInformation  requestRegist
     * 请求历史消息--发送HistoryMessage对象 requestHistoryMessage
     * 请求返回列表信息
     * 发送信息--发送OnlineMessage对象 sendMessage
     * 返回UI是否允许登录信息-建议使用Boolean类型 acceptLogin
     * 返回UI是否注册成功信息-建议使用String类型 acceptRegister
     * 返回UI离线消息--使用List<OfflineMessage>返回 returnOfflineMessage
     * 返回UI历史消息--使用List<HistoryMessage>返回 returnHistoryMessage
     * 返回UI在线消息--使用OnlineMessage返回 returnOnlineMessage
     *
     */
    public static void requestLogin(User user) throws IOException {
        objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectOutputStream.writeObject(user);
        objectOutputStream.flush();

    }
    public static void requestRegister(UserInformation userInformation) throws IOException {
        objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectOutputStream.writeObject(userInformation);
        objectOutputStream.flush();

    }
    public static void requestHistoryMessage() throws IOException {
        objectOutputStream.writeObject(new HistoryMessage(null,null,null,null));
        objectOutputStream.flush();

    }
    public static void sendMessage(OnlineMessage onlineMessage) throws IOException {

        objectOutputStream.writeObject(onlineMessage);
        objectOutputStream.flush();

    }
    public static void sendDocument(UserDocument userDocument) throws IOException {
        objectOutputStream.writeObject(new UserDocument(null,null,null));
        objectOutputStream.writeObject(userDocument);
        objectOutputStream.flush();

    }

    public static UserDocument getUserDocumentByPath(String receiver, String filePath,String fileName) throws IOException {
        DataInputStream dataInputStream = new DataInputStream(new FileInputStream(filePath));
        byte[] buf = new byte[1024 * 100];
        dataInputStream.read(buf);
        return new UserDocument(receiver,buf,fileName);
    }

}

class ReceiveThread extends Thread{
    ObjectInputStream objectInputStream;

    public ReceiveThread(ObjectInputStream objectInputStream) {
        this.objectInputStream = objectInputStream;
    }

    @Override
    public void run() {
        while(true){
            try {
                Object object = objectInputStream.readObject();
                if("Find error in user regist!".equals(object)){
                    RegisteJFrame.registrationReturnInformation("Find error in user regist!");
                }else if("user created successfully!".equals(object)){
                    RegisteJFrame.registrationReturnInformation("user created successfully!");
                }else if("Login successful！".equals(object)){
                    Login.changePermission(2);
                }else if("Login error！".equals(object)){
                    Login.changePermission(1);
                }else if("class user.User".equals(object.getClass().toString())){
                    User tempUser;
                    ArrayList<User> personList = new ArrayList<>();
                    while(true){
                        tempUser = (User) objectInputStream.readObject();
                        if(tempUser.getUserName()==null) {
                            break;
                        }
                        personList.add(tempUser);
                    }
                    ContactJFrame.returnListInformation(personList);
                    ContactJFrame.setButton();
                }else if("class message.OfflineMessage".equals(object.getClass().toString())){
                    Message tempMessage;
                    ArrayList<OfflineMessage> offlineMessageArrayList = new ArrayList<>();
                    while(true){
                        tempMessage = (Message)objectInputStream.readObject();
                        if(tempMessage.getSender()==null) {
                            break;
                        }
                        OfflineMessage tempOfflineMessage = (OfflineMessage)tempMessage;
                        offlineMessageArrayList.add(tempOfflineMessage);
                        ContactJFrame.changeButton(tempMessage);
                    }
                    if(offlineMessageArrayList.size()!=0){
                        File file = new File("信息提示.mp3");
                        FileInputStream fileInputStream = new FileInputStream(file);
                        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
                        Player player = new Player(bufferedInputStream);
                        player.play();
                    }
                    ContactJFrame.returnOfflineMessage(offlineMessageArrayList);

                }else if("class message.OnlineMessage".equals(object.getClass().toString())){
                    OnlineMessage onlineMessage = (OnlineMessage)objectInputStream.readObject();
                    File file = new File("信息提示.mp3");
                    FileInputStream fileInputStream = new FileInputStream(file);
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
                    Player player = new Player(bufferedInputStream);

                    player.play();
                    ContactJFrame.returnOnlineMessage(onlineMessage);

                }else if("class message.HistoryMessage".equals(object.getClass().toString())){
                    Object o;
                    ArrayList<HistoryMessage> historyMessageArrayList = new ArrayList<>();
                    while(true){
                        o = objectInputStream.readObject();
                        HistoryMessage t = (HistoryMessage)o;
                        if(t.getSender()==null) {
                            break;
                        }
                        historyMessageArrayList.add(t);
                    }
                    ContactJFrame.returnHistoryList(historyMessageArrayList);
                }
                else if("class user.UserDocument".equals(object.getClass().toString()))
                {
                    Object o=objectInputStream.readObject();
                    UserDocument userDocument=(UserDocument) o;

                    ContactJFrame.receiveUserDocument(userDocument);
                }
            } catch (IOException | ClassNotFoundException | JavaLayerException e) {
                e.printStackTrace();
                return;
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}