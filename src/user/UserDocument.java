package user;

import java.io.Serializable;

/**
 * @author ������
 */
public class UserDocument implements Serializable {

    private String receiver;
    private byte[] sendbutes;
    private String fileName;

    public UserDocument(String receiver, byte[] sendbutes,String fileName) {
        this.receiver = receiver;
        this.sendbutes = sendbutes;
        this.fileName= fileName;
    }

    public String getReceiver() {
        return receiver;
    }

    public byte[] getSendbutes() {
        return sendbutes;
    }

    public String getFileName() {
        return fileName;
    }
}