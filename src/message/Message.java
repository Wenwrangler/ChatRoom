package message;

import java.io.Serializable;

/**
 * @author �º���
 */

public class Message implements Serializable {

    private String time;
    private String sender;

    private String receivingEnd;


    private String message;
    public Message(String time, String sender, String receivingEnd, String message) {
        this.time = time;
        this.sender = sender;
        this.receivingEnd = receivingEnd;
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public String getSender() {
        return sender;
    }

    public String getReceivingEnd() {
        return receivingEnd;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "time='" + time + '\'' +
                ", sender='" + sender + '\'' +
                ", receivingEnd='" + receivingEnd + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
