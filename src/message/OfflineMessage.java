package message;

import java.io.Serializable;

/**
 * @author �º���
 */

public class OfflineMessage extends Message implements Serializable {

    public OfflineMessage(String time, String sender, String receivingEnd, String message) {
        super(time, sender, receivingEnd, message);
    }
}
